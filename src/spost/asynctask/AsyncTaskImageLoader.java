package spost.asynctask;

import spost.helper.CacheHelper;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 非同期処理クラス
 */

/**
 * 1. クラス作成：AsyncTask<T1, T2, T3>
 *
 * T1 => スレッドが開始されたときに、渡す引数の型。(doInBackgroundメソッドの引数)
 * T2 => 進捗率を表示させるときに使う型。(onProgressUpdateメソッドの引数)
 * T3 => スレッド終了時に値を返す時の型。(onPostExecuteの引数・doInBackgroundの返り値）
 * ※上記３つのクラスは不要であればjava.lang.Voidを指定すればOK
 */

/**
 * 2. メソッド実装
 *
 * ↓メソッドを追加する(doInBackgroundが最低限実装されてればいい)
 * ・doInBackground
 * 　バックグラウンドで実行させたい処理。
 * ・onPreExecute
 * 　タスク開始直後に呼ばれる。
 * ・onProgressUpdate
 * 　プログレスバーとかに進捗具合を更新したい場合はこの中に記述する。
 * ・onPostExecute
 * 　スレッドが終了した後に呼ばれる。「完了！」とかTextViewなどに表示させたい場合はここに記述する。
 *
 */

public class AsyncTaskImageLoader extends AsyncTask<String, Void, Bitmap> {

	private Context   mContext;
	private CacheHelper mCacheHelper;

	/**
	 * コンストラクタ
	 * @param imageView  取得したBitmapをバインドするImageView
	 * @param progress   取得待機中にローディングするProgressBar
	 * @param blankView  取得に失敗した場合のImageView
	 */
	public AsyncTaskImageLoader(Context context) {
		mContext        = context;
	}

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Bitmap doInBackground(String... params) {
		Bitmap bitmap = null;
		String url    = params[0];

		// URLなし
		if (url == null) return bitmap;

		// キャッシュ検索: HIT
		bitmap = this.getCacheHelper().get(url);
		if (bitmap != null) return bitmap;

		// 指定URLからBitmap取得
		bitmap = this.getBitmapFromURL(url);

		return bitmap;
	}

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
	}


	/**
	 * 指定URLからBitmap取得
	 * @param url
	 * @return
	 */
	private Bitmap getBitmapFromURL(String url) {
	    Bitmap bitmap         = null;
		HttpGet httpRequest   = new HttpGet(url);
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpResponse response;

		try {
	    	// 指定URLの画像データ(Bitmap)取得
	        response = (HttpResponse) httpclient.execute(httpRequest);

			switch (response.getStatusLine().getStatusCode()) {
			// 200 OK
			case HttpStatus.SC_OK:
		        HttpEntity entity = response.getEntity();
		        BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
		        InputStream instream = bufHttpEntity.getContent();
		        bitmap = BitmapFactory.decodeStream(instream);

		        // キャッシュへBitmapを保存
		        this.getCacheHelper().put(url, bitmap);
			// 404 NOT FOUND
			case HttpStatus.SC_NOT_FOUND:
				// データ取得失敗時のロギング等
			}
	    } catch (ClientProtocolException e) {
	        e.printStackTrace();
	        Log.v("CHECK", "failed!" + url);
	    } catch (IOException e) {
	        e.printStackTrace();
	        Log.v("CHECK", "failed!" + url);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    } finally {
	    	// HttpClientのコネクション切断
	    	httpclient.getConnectionManager().shutdown();
	    }

	    return bitmap;
	}

	/**
	 * キャッシュ管理クラス取得
	 * @return
	 */
	private CacheHelper getCacheHelper() {
		if (mCacheHelper == null) {
			mCacheHelper = new CacheHelper(mContext);
		}
		return mCacheHelper;
	}
}