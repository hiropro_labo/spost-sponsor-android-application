package spost.item;

import android.widget.ImageView;

public class MainteItem {

	private String mAlive;
	private String mCopyright;
	private String mAppName;
	private String mColor;
	
	private ImageView mAppImg;
	private ImageView mMenuIcon;
	private ImageView mBackIcon;
	
	/**
	 * コンストラクタ
	 */
	public MainteItem() {
		initialize();
	}
	
	/**
	 * セッター
	 * @param
	 */
	public void setAlive(String alive) {
		this.mAlive = alive;
	}
	
	public void setCopyright(String copyright) {
		this.mCopyright = copyright;
	}

	public void setAppName(String appName) {
		this.mAppName = appName;
	}

	public void setColor(String color) {
		this.mColor = color;
	}

	public void setAppImage(ImageView appImg) {
		this.mAppImg = appImg;
	}
	
	public void setMenuIcon(ImageView menuicon) {
		this.mMenuIcon = menuicon;
	}
	
	public void setBackIcon(ImageView backIcon) {
		this.mBackIcon = backIcon;
	}


	/**
	 * ゲッター
	 * @return
	 */
	public String getAlive() {
		return this.mAlive;
	}

	public String getCopyright() {
		return this.mCopyright;
	}

	public String getAppName() {
		return this.mAppName;
	}

	public String getColor() {
		return this.mColor;
	}

	public ImageView getAppImage() {
		return this.mAppImg;
	}

	public ImageView getMenuIcon() {
		return this.mMenuIcon;
	}

	public ImageView getBackIcon() {
		return this.mBackIcon;
	}
	
	
	/**
	 * メンテナンス情報初期化
	 */
	public void initialize(){
		mAlive     = "";
		mCopyright = "";
		mAppName   = "";
		mColor     = "";
		
		mAppImg    = null;
		mMenuIcon  = null;
		mBackIcon  = null;
	}
	
	/**
	 * メンテナンス項目「alive」チェック
	 * @return boolean
	 */
	public boolean checkAliveStatus(){
		
		boolean checkFlg = false; 
		
		if(this.getAlive() != null &&  this.getAlive() != ""){
			
			if (this.getAlive().equals("0") || this.getAlive().equals("1") || this.getAlive().equals("2")) {
				checkFlg = true;
			}
		}
		
		return checkFlg;
	}
	
}