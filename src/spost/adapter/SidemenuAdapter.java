﻿package spost.adapter;

import jp.co.hiropro.spost.test_sponsor.R;
import spost.item.SidemenuItem;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SidemenuAdapter extends ArrayAdapter<SidemenuItem> {
	
	
	private ArrayList<SidemenuItem> menuItem;
	private LayoutInflater inflater;
	private Resources res;
	
	/**
	 * コンストラクタ
	 *
	 * @param context
	 * @param objects
	 */
	public SidemenuAdapter(Context context, int resID, ArrayList<SidemenuItem> items) {
		super(context, resID, items);
		this.menuItem = items;
		this.res = context.getResources();
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * 行ごとのViewを生成
	 *
	 * @param position
	 * @param convertView
	 * @param parent
	 */
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
    	
    	View sideMenuView = convertView;
    	if (sideMenuView == null) {
    		sideMenuView = inflater.inflate(R.layout.menulist_view, null);
    	}
    	
    	LinearLayout menulistContainer = (LinearLayout) sideMenuView.findViewById(R.id.menulist_container);
    	
		SidemenuItem mSideMenu = menuItem.get(position);

	    // メニューリストの色を交互に変更
		if (position % 2 == 0) {
			menulistContainer.setBackgroundColor(res.getColor(R.color.list_oddnum_color));
		} else {
			menulistContainer.setBackgroundColor(res.getColor(R.color.list_evennum_color));
		}
		
		// 文字列をセット
	    TextView sidemenuText = (TextView)sideMenuView.findViewById(R.id.menulist_title);
	    sidemenuText.setText(mSideMenu.getName());
	    
	    // アイコンをセット
	    ImageView sidemenuImage = (ImageView)sideMenuView.findViewById(R.id.menulist_icon);
	    if (mSideMenu.getIcon() != null) {
	    	sidemenuImage.setImageDrawable(mSideMenu.getIcon().getDrawable());
	    } else {
	    	sidemenuImage.setImageResource(R.drawable.no_image);
	    }
	    
		return sideMenuView;
	}
}