package spost.api;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import jp.co.hiropro.spost.test_sponsor.Globals;
import spost.asynctask.AsyncTaskImageLoader;
import spost.item.MainteItem;

/**
 * @author yoshitaka
 */
public class ServerAPIMainte extends ServerAPIAbstract {

	private static Globals sGlobals;

	/**
	 * コンストラクタ
	 * @param context
	 */
	public ServerAPIMainte(Context context) {
		super(context);
	}

	/**
	 * ServerAPIMainteインスタンス取得
	 * @param context
	 * @param globals
	 * @return
	 */
	public static ServerAPIMainte getInstance(Context context, Globals globals) {
		sGlobals = globals;
		ServerAPIMainte serverAPI = new ServerAPIMainte(context);
		return serverAPI;
	}

	@Override
	String getPrefName() {
		return sGlobals.getPrefKeyName();
	}

	@Override
	String getPrefKeyData() {
		return sGlobals.getPrefKeyMainte();
	}

	@Override
	String getApiURLData() {
		return sGlobals.getApiMainte();
	}

	@Override
	HashMap<String, String> getApiURLParamsData() {
		HashMap<String, String> params = new HashMap<String, String>();
		return params;
	}
	
	/**
	 * メンテナンス情報の取得
	 * @return
	 */
	public MainteItem getMainteItem() {
		
		// ＪＳＯＮデータ取得
		JSONObject mJsObjMainte = null;
		try {
			mJsObjMainte = new JSONObject((String) this.getCurrentData());
		} catch (JSONException e) {
			Log.v("YOSHITAKA", "MAINTE ITEM : getMainteItem : error : " + e.getMessage());
		}
		
		
		String mIconUrl = "";
		ImageView mImageView;
		
		JSONObject mJsObjBar = null;
		MainteItem mMainteItem = new MainteItem();
		
		if (mJsObjMainte != null) {
			
			if (mJsObjMainte.isNull("alive") != true) {
				mMainteItem.setAlive(mJsObjMainte.optString("alive"));
			}
			if (mJsObjMainte.isNull("copyright") != true) {
				mMainteItem.setCopyright(mJsObjMainte.optString("copyright"));
			}
			if (mJsObjMainte.isNull("app_name") != true) {
				mMainteItem.setAppName(mJsObjMainte.optString("app_name"));
			}
			// アプリタイトル画像取得
			if (mJsObjMainte.isNull("app_icon") != true) {
				mIconUrl = sGlobals.getApiImg() + mJsObjMainte.optString("app_icon");
				mImageView = this.getIconView(mIconUrl);
				mMainteItem.setAppImage(mImageView);
			}
			
			// タイトルバー情報取得(オブジェクト)
			mJsObjBar = mJsObjMainte.optJSONObject("status_bar");
		}
		
		if (mJsObjBar != null) {
			
			if (mJsObjBar.isNull("color") != true) {
				mMainteItem.setColor(mJsObjBar.optString("color"));
			}
			// メニューアイコン取得
			if (mJsObjBar.isNull("btn_menu") != true) {
				mIconUrl = sGlobals.getUrlImgGen() + mJsObjBar.optString("btn_menu");
				mImageView = this.getIconView(mIconUrl);
				mMainteItem.setMenuIcon(mImageView);
			}	
			// 戻るアイコン取得
			if (mJsObjBar.isNull("btn_back") != true) {
				mIconUrl = sGlobals.getUrlImgGen() + mJsObjBar.optString("btn_back");
				mImageView = this.getIconView(mIconUrl);
				mMainteItem.setBackIcon(mImageView);
			}	
		}
		
		return mMainteItem;
	}
	
	private ImageView getIconView(String url) {
		
		AsyncTaskImageLoader task = new AsyncTaskImageLoader(mContext);
		task.execute(url);
		ImageView mImageView = new ImageView(mContext);
		try {
			mImageView.setImageBitmap(task.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
			mImageView = null;
		} catch (ExecutionException e) {
			e.printStackTrace();
			mImageView = null;
		}
        
        return mImageView;
	}

}