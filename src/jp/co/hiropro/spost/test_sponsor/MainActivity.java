package jp.co.hiropro.spost.test_sponsor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.android.gcm.GCMRegistrar;

import jp.co.hiropro.spost.test_sponsor.R;
import spost.adapter.SidemenuAdapter;
import spost.api.ServerAPIMainte;
import spost.api.ServerAPISidemenu;
import spost.item.MainteItem;
import spost.item.SidemenuItem;

public class MainActivity extends Activity implements OnClickListener, OnItemClickListener {
	
	// フラグ
	private boolean isOpenMenuFlg = false;
	
	// ディスプレイ関連
	private int mDispWidth = 0;
	private int mDispHeight = 0;
	private int mScrollX = 0;
		
	private RelativeLayout mainFrame;
	private LinearLayout menuFrame;
	private ListView menuList;
	private LinearLayout socialList;
	private FrameLayout webFrame;
	private ImageButton backBtn;

	private WebView mainWeb;
	
	private Globals sGlobals;
	private ServerAPIMainte apiMainte;
	private ServerAPISidemenu apiSideMenu;
	private MainteItem mMainteItem;
	private ArrayList<SidemenuItem> mAryMenuList;
	private HashMap<String, String> hsMpSocialItem;
	
	/**
	 * イベント(アクティビティ作成時)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);
        
        // 項目取得初期処理
        sGlobals = Globals.getInstance();
		this.getMenuItem();
        
        // スライド関連初期処理
		this.getDispItem();
 
        // レイアウト関連初期処理
        // メインフレーム、メニューフレーム        
        mainFrame = (RelativeLayout)findViewById(R.id.mainFrame);
        menuFrame = (LinearLayout)findViewById(R.id.menuFrame);
        // ヘッダー
        this.setHeader();
		
        // メニューフレーム内表示情報初期処理
        menuList = (ListView)findViewById(R.id.menuList);
        menuList.setOnItemClickListener(this);
        socialList = (LinearLayout)findViewById(R.id.socialList);
        this.createMenuList();
        menuFrame.setVisibility(View.INVISIBLE);

        
        // ＴＯＰページの表示
        webFrame = (FrameLayout)findViewById(R.id.container);
		webFrame.removeAllViews();
		mainWeb = createWebView("top");
		webFrame.addView(mainWeb);
    }

	/**
	 * イベント(ボタン押下時)
	 */
    @Override
	public void onClick(View v) {
        
		switch (v.getId()) {
		// 「戻る」ボタン (ヘッダー)
			case R.id.backButton:
				if(mainWeb.canGoBack() == true) {
					webFrame.removeAllViews();
					mainWeb.goBack();
					webFrame.addView(mainWeb);
				}
				break;
			// 「メニュー」ボタン (ヘッダー)
			case R.id.menuButton:
				if (isOpenMenuFlg == false) {
					slideInMenu();
				} else {
					slideOutMenu();
				}
				break;
		}
	}

	/**
	 * イベント(メニューリスト項目押下時)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		slideOutMenu();
		
		int mSeqNumber = 0;
		String mController = "";
		
		for (int i = 0; i < mAryMenuList.size(); i ++) {
			
			if (position + 1 == mAryMenuList.get(i).getSequence()){
				mController = mAryMenuList.get(i).getController();
				mSeqNumber = i;
			}
			
		}
		
		
		if (mController.equals("tel") == true) {
		    // 電話アプリ起動
			webFrame.removeAllViews();
			webFrame.addView(mainWeb);
			
			Uri mTelUri = Uri.parse("tel:" + mAryMenuList.get(mSeqNumber).getTel());
			startActivity(new Intent(Intent.ACTION_DIAL,mTelUri));
			
		} else if (mController.equals("email") == true) {
		    // メールアプリ起動
			webFrame.removeAllViews();
			webFrame.addView(mainWeb);
			
			Uri mMailUri = Uri.parse ("mailto:" + mAryMenuList.get(mSeqNumber).getEMail());
			Intent mIntent = new Intent(Intent.ACTION_SENDTO, mMailUri);   
			mIntent.putExtra(Intent.EXTRA_SUBJECT, sGlobals.getMailTitle());  
			mIntent.putExtra(Intent.EXTRA_TEXT, sGlobals.getMailText());  
			startActivity(mIntent);  
		} else if (mController.equals("url") == true) {
		    // 該当ＷＥＢページのブラウザ表示
			webFrame.removeAllViews();
			webFrame.addView(mainWeb);
			
			Uri mUri = Uri.parse(mAryMenuList.get(position).getUrl());
			startActivity(new Intent(Intent.ACTION_VIEW, mUri));
		} else {
		    // 該当ＷＥＢページの表示
			webFrame.removeAllViews();
			mainWeb = createWebView(mController);
			webFrame.addView(mainWeb);
		}
	}
	
	/**
	 * イベント(別のActivity開始時)
	 */
	@Override
	public void onPause(){
		super.onPause();

		if (isOpenMenuFlg == true) {
			slideOutMenu();
		}
		
		webFrame.removeAllViews();
		webFrame.addView(mainWeb);
	}
	
	/**
	 * イベント(端末のＢＡＣＫボタン押下時)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		// Backキーが押された時に終了確認ダイアログを出す。
		if(keyCode==KeyEvent.KEYCODE_BACK) {
			// アラートダイアログ
			showDialog(0);
			return true;
		}
		return false;
	}

	/**
	 *  イベント(ダイアログ作成時)
	 */
	@Override
	public Dialog onCreateDialog(int id) {
		
		switch (id) {
			case 0:
				//ダイアログの作成(AlertDialog.Builder)
				return new AlertDialog.Builder(MainActivity.this)
				.setMessage("「アプリ」を終了しますか?")
				.setCancelable(false)
				// 「終了する」が押された時の処理
				.setPositiveButton("終了する", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// アクティビティ消去
						MainActivity.this.finish();
					}
				})
				// 「終了しない」が押された時の処理
				.setNegativeButton("終了しない", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {}
			})
			.create();
		}
		
		return null;
	}

	/**
	 * サイドメニューの項目取得
	 */
	public void getMenuItem() {

		apiSideMenu = ServerAPISidemenu.getInstance(getApplication(), sGlobals);
		
		mAryMenuList = apiSideMenu.getSideMenuList();
		hsMpSocialItem = apiSideMenu.getSocialItem();
		
		apiMainte = ServerAPIMainte.getInstance(getApplication(), sGlobals);
		mMainteItem = apiMainte.getMainteItem();		
	}

	/**
	 * スライド関連項目取得
	 */
	public void getDispItem() {
		
        DisplayMetrics mMetrics = new DisplayMetrics();
        Display mDisp = getWindowManager().getDefaultDisplay();        
        mDisp.getMetrics(mMetrics);
        mDispWidth  = mDisp.getWidth();
        mDispHeight = mDisp.getHeight();
        mScrollX       = mDispWidth - (int)(mMetrics.scaledDensity * 100);
		
	}
	
	/**
	 * ヘッダーの設定
	 */
	public void setHeader() {
		
		// ヘッダー
        FrameLayout mHeaderFrame = (FrameLayout)findViewById(R.id.headerFrame);
        this.getLayoutInflater().inflate(R.layout.header_view, mHeaderFrame);
        mHeaderFrame.setBackgroundColor(Color.parseColor(mMainteItem.getColor()));

        ImageButton mMenuBtn = (ImageButton)findViewById(R.id.menuButton);
        if (mMainteItem.getMenuIcon() != null) {
//        	mMenuBtn.setImageDrawable(mMainteItem.getMenuIcon().getDrawable());
        	mMenuBtn.setBackgroundDrawable(mMainteItem.getMenuIcon().getDrawable());
        } else  {
        	mMenuBtn.setImageResource(R.drawable.button_menu);
        }
        mMenuBtn.setOnClickListener(this);
        
        backBtn = (ImageButton)findViewById(R.id.backButton);
        if (mMainteItem.getBackIcon() != null) {
//        	backBtn.setImageDrawable(mMainteItem.getBackIcon().getDrawable());
        	backBtn.setBackgroundDrawable(mMainteItem.getBackIcon().getDrawable());
        } else  {
        	backBtn.setImageResource(R.drawable.button_back);
        }
        backBtn.setVisibility(View.INVISIBLE);
        backBtn.setOnClickListener(this);
        
        FrameLayout mTitle = (FrameLayout)findViewById(R.id.title);
        if (mMainteItem.getAppImage() != null) {
        	ImageView mTitleImg = mMainteItem.getAppImage();
        	mTitle.addView(mTitleImg);
        } else {
        	TextView mTitleTxt = new TextView(this);
        	mTitleTxt.setText(mMainteItem.getAppName());
        	mTitleTxt.setGravity(Gravity.CENTER);
        	mTitle.addView(mTitleTxt);
        }
	}
	
	/**
	 * サイドメニュー作成(メニューリスト ＋ ＳＮＳリスト)
	 */
	private void createMenuList() {
		
		// メニューリスト作成
		menuList.setAdapter(new SidemenuAdapter(getApplication(), R.layout.menulist_view, mAryMenuList));
		
		// ＳＮＳリスト作成(取得できたＳＮＳ情報のみ)
		final String socialItemList[] = sGlobals.getSocialList();
		
		for (int i=0; i < socialItemList.length; i++) {
			
			// カウント項目退避
			final int count = i;
			
			if (hsMpSocialItem.containsKey(socialItemList[i]) == true) {
				
				final ImageButton mSnsBtn = new ImageButton(this);
				
				// ボタンレイアウト設定
				LinearLayout.LayoutParams mBtnLayout = new LinearLayout.LayoutParams(
						getResources().getDimensionPixelSize(R.dimen.social_item_width),
						getResources().getDimensionPixelSize(R.dimen.social_item_height));
				mBtnLayout.setMargins(getResources().getDimensionPixelSize(R.dimen.social_item_margin),
									  getResources().getDimensionPixelSize(R.dimen.social_item_margin),
									  0,
									  getResources().getDimensionPixelSize(R.dimen.social_item_margin));
				mSnsBtn.setLayoutParams(mBtnLayout);
				mSnsBtn.setBackgroundColor(getResources().getColor(R.color.clear));
				// ボタン画像設定
				if        (socialItemList[i] == "facebook") {
					mSnsBtn.setImageResource(R.drawable.icon_facebook);
				} else if (socialItemList[i] == "twitter") {
					mSnsBtn.setImageResource(R.drawable.icon_twitter);
				} else if (socialItemList[i] == "line") {
					mSnsBtn.setImageResource(R.drawable.icon_line);
				} else if (socialItemList[i] == "youtube") {
					mSnsBtn.setImageResource(R.drawable.icon_youtube);
				} else {
					mSnsBtn.setImageResource(R.drawable.no_image);
				}
				
				mSnsBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						slideOutMenu();
						
						Uri socialUri = Uri.parse(hsMpSocialItem.get(socialItemList[count]));
						startActivity(new Intent(Intent.ACTION_VIEW, socialUri));
					}
				});
				
				socialList.addView(mSnsBtn);
			}
		}	
	}

	/**
	 * WebView表示
	 * @param position
	 */
	@SuppressLint("SetJavaScriptEnabled")
	private WebView createWebView(String controller) {
		
		String mUrl = "";
		final String mToken = GCMRegistrar.getRegistrationId(this);
		
		final WebView mWebView = new WebView(MainActivity.this);
		//JavaScript有効
		mWebView.getSettings().setJavaScriptEnabled(true);
		//スクロールバー無効
		mWebView.setVerticalScrollBarEnabled(false);
		mWebView.setHorizontalScrollBarEnabled(false);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
		
		mWebView.setWebViewClient(new WebViewClient(){
			
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url ) {
				
				String reqStr = url;
				String mPost = "token=" + mToken;
				
				if (reqStr.startsWith("http://app.spost.jp/news/detail/")) {
					mWebView.postUrl(reqStr, mPost.getBytes());
				}
				if (reqStr.equals("app://news.detail/XXXX/")) {
					String sendUrl = "http://api.spost.jp/news/cnt/";
					mWebView.postUrl(sendUrl, mPost.getBytes());
					
					return false;
				}
				
				return true;
			}
			
			@Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                
                if(view.canGoBack() == true) {
                	backBtn.setVisibility(View.VISIBLE);
        		} else {
        			backBtn.setVisibility(View.INVISIBLE);
        		}
            }
		});
		
		long mCurTimeM = System.currentTimeMillis();
		String mCurTimeMStr = String.valueOf(mCurTimeM);

		mUrl = sGlobals.getUrlApp() + controller + "/" + sGlobals.getClientID() + "?" + mCurTimeMStr;
		
		if (controller.equals("setting") == true) {
			mUrl = sGlobals.getUrlApp() + controller + "/" + 
				   sGlobals.getClientID() + "/" + mToken + "?" + mCurTimeMStr;
		}

		mWebView.loadUrl(mUrl);
		
		return mWebView;

	}
	
	/**
	 * サイドメニュースライドイン
	 */
	private void slideInMenu() {
		
		isOpenMenuFlg = true;
		menuFrame.setVisibility(View.VISIBLE);
		
		TranslateAnimation mTranAnim = new TranslateAnimation(0, -mScrollX, 0, 0);
		mTranAnim.setAnimationListener(new AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
				// スライドメニュー設置位置(スライドイン後)
				menuFrame.layout(mDispWidth - mScrollX, 0, mDispWidth * 2 - mScrollX, mDispHeight);
			}
			
			public void onAnimationRepeat(Animation animation) {}
			
			public void onAnimationEnd(Animation animation) {
				// メイン画面設置位置(スライドイン後)
				mainFrame.layout(-mScrollX, 0, mDispWidth - mScrollX, mDispHeight);
				mainFrame.setAnimation(null);
			}
		});
		
		mTranAnim.setDuration(400);
		mainFrame.startAnimation(mTranAnim);
	}
	

	/**
	 * サイドメニュースライドアウト
	 */
	private void slideOutMenu() {
		
		isOpenMenuFlg = false;
		menuFrame.setVisibility(View.INVISIBLE);

		TranslateAnimation mTranAnim = new TranslateAnimation(-mScrollX, 0, 0, 0);
		mTranAnim.setAnimationListener(new AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
				// メイン画面設置位置(スライドアウト後)
				mainFrame.layout(0, 0, mDispWidth, mDispHeight);
			}
			
			public void onAnimationRepeat(Animation animation) {}
			
			public void onAnimationEnd(Animation animation) {
				// スライドメニュー設置位置(スライドアウト後)
				menuFrame.layout(mDispWidth, 0, mDispWidth + mScrollX, mDispHeight);
				mainFrame.setAnimation(null);
			}
		});
		
		mTranAnim.setDuration(400);
		mainFrame.startAnimation(mTranAnim);
	}

}