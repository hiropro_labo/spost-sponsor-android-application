package jp.co.hiropro.spost.test_sponsor;

import static spost.utilities.CommonUtilities.SENDER_ID;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.concurrent.ExecutionException;

import com.google.android.gcm.GCMRegistrar;

import jp.co.hiropro.spost.test_sponsor.Globals;
import jp.co.hiropro.spost.test_sponsor.R;
import spost.api.ServerAPIMainte;
import spost.asynctask.AsyncTaskMainte;
import spost.item.MainteItem;

public class SplashActivity extends Activity {
	
	int Alive;
	
	boolean conectedFlg = false;
	boolean loadedFlg = false;
	
	private AlertDialog.Builder mAlertDialogBuilder;
	private AsyncTask<Void, Void, Void> mRegisterTask;
	
	private Globals sGlobals;
	private AsyncTaskMainte mAsyncTaskMainte;
	private ServerAPIMainte apiMainte;
	private MainteItem mMainteItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_view);

		// 初期化処理
		sGlobals = Globals.getInstance();

		// APIサーバーからデータ取得
		mAsyncTaskMainte = new AsyncTaskMainte(getApplication(), sGlobals);
		mAsyncTaskMainte.execute();

		// スプラッシュ画面表示後にメイン画面自動遷移
		Handler handler = new Handler();
		handler.postDelayed(new splashHandler(), 500);
	}

	/**
	 * メイン画面自動遷移(事前チェック＆遷移)
	 */
	class splashHandler implements Runnable {
		public void run() {

			if(SplashActivity.this.isConnected() == false){
				//ネットワーク接続エラー表示
				SplashActivity.this.isconnectedErrorDialog();
			}
			
			if(SplashActivity.this.waitLoaded() == false){
				//API読み込み失敗エラー表示
				SplashActivity.this.apiErrorDialog();
			}

			//メンテナンスチェック
			SplashActivity.this.MainteCheck();
           
			// GCM関連処理
			SplashActivity.this.initGCM();
						
			//画面遷移
			SplashActivity.this.moveToMainActivity();
		}
	}
	
	/**
	 * ネットワーク接続チェック
	 * @return
	 */
	private boolean isConnected() {
		//ネットワーク接続エラーフラグ
		boolean conectedFlg;
		
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		
		if (netInfo != null) {
			conectedFlg = netInfo.isConnected();
		} else {
			conectedFlg = false;
		}
		
		return conectedFlg;
	}
	
	/**
	 * ＡＰＩ更新完了チェック
	 */
	private boolean waitLoaded(){
		
		try{
			loadedFlg = mAsyncTaskMainte.get();
		} catch (InterruptedException e) {
			loadedFlg = false;
		} catch (ExecutionException e) {
			loadedFlg = false;
		}
		
		return loadedFlg;
	}
	
	/**
	 * ネットワーク接続エラーダイアログ
	 */
	private void isconnectedErrorDialog(){
		AlertDialog.Builder builder = this.getAlertDialogBuilder();

		// ダイアログ表示		
		builder.setTitle("ネットワーク接続エラー");
		builder.setMessage("一部ご利用できない機能がございます。\nネットワークに接続の上、アプリケーションの起動をお願い致します。");
		builder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(getApplication(), SplashActivity.class);
				startActivity(intent);
				SplashActivity.this.finish();
			}
		});
		builder.create().show();
	}
	
	/**
	 * API読み込み失敗時のダイアログ
	 */
	private void apiErrorDialog(){
		AlertDialog.Builder builder = this.getAlertDialogBuilder();

		// AlertDialog表示
		builder.setTitle("ネットワーク接続エラー");
		builder.setMessage("一部ご利用できない機能がございます。\nネットワークに接続の上、アプリケーションの起動をお願い致します。");
		builder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent intent = new Intent(getApplication(), SplashActivity.class);
				startActivity(intent);
				SplashActivity.this.finish();
			}
		});
		builder.create().show();
	}
	
	/**
	 * メンテナンス状況チェック
	 */
	private void MainteCheck(){
		
		apiMainte = ServerAPIMainte.getInstance(getApplication(), sGlobals);
		mMainteItem = apiMainte.getMainteItem();
		
		if (mMainteItem.checkAliveStatus() == true) {
			Alive = Integer.valueOf(mMainteItem.getAlive());
		} else {
			Alive = 1;
		}
		
		//メンテナンス情報チェック
		switch (Alive) {
			case 0:
				// 正常
				break;
			case 1:
				// メンテナンス中
				AlertDialog.Builder builder = SplashActivity.this.getAlertDialogBuilder();
				builder.setTitle("お知らせ");
				builder.setMessage("現在メンテナンス中になっており、一部ご利用できない機能がございます。\n" +
						"ご了承下さい。");
				builder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						SplashActivity.this.finish();
					}
				});
				builder.create().show();
				break;
			case 2:
				// 顧客都合により停止中
				builder = SplashActivity.this.getAlertDialogBuilder();
				builder.setTitle("お知らせ");
				builder.setMessage("現在お客様都合により、停止中となっております。\n" +
						"ご了承下さい。");
				builder.setPositiveButton("閉じる", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						SplashActivity.this.finish();
					}
				});
				builder.create().show();
				break;
		}
	}
	
	/**
	 * GCM関連処理
	 * ※暫定的にメソッド切り分け
	 */
	private void initGCM() {
		Log.v("YOSHITAKA", "デバイス登録...");

		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		
		final String regId = GCMRegistrar.getRegistrationId(this);
		
		if (regId.equals("")) {
			GCMRegistrar.register(this, SENDER_ID);
		} else {
//			if (GCMRegistrar.isRegisteredOnServer(this)) {} else {}
			final Context context = this;
			mRegisterTask = new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					boolean registered = ServerUtilities.register(context, regId);
					if (!registered) {
						GCMRegistrar.unregister(context);
					}
					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					mRegisterTask = null;
				}
			};
			mRegisterTask.execute(null, null, null);
		}
	}
	
	/**
	 * 画面遷移処理
	 * ※ネットワーク通信が不可の場合はアラート表示
	 */
	private void moveToMainActivity()
	{	
		Intent intent = new Intent(getApplication(), MainActivity.class);
		
		//スプラッシュ完了後に実行するActivityを指定します。
		startActivity(intent);
		SplashActivity.this.finish();
	}

	/**
	 * AlertDialogBuilderインスタンス取得
	 * @return
	 */
	private AlertDialog.Builder getAlertDialogBuilder() {
		if (mAlertDialogBuilder == null) {
			mAlertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);
			mAlertDialogBuilder.setPositiveButton("OK", null);
		}
		return mAlertDialogBuilder;
	}
}
