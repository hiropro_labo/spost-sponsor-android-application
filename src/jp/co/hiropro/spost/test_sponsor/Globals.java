package jp.co.hiropro.spost.test_sponsor;

import android.app.Application;

public class Globals extends Application  {
	
// Start プロジェクトで共通のインスタンスにする処理（シングルトンパターン）
	private static Globals glinstance = new Globals();
	
	private Globals() {
		// クライアントＩＤ初期化
		_client_id = "4";	
		
	}  
	
	public static Globals getInstance() {  
		return glinstance;  
	}
//  End  プロジェクトで共通のインスタンスにする処理（シングルトンパターン）
	
	/**
	 * 宣言群
	 */
	// クライアントID
	final private String _client_id;
	
	// 接続先：本番環境
	final private String _url_api     = "http://api.spost.jp/";
	final private String _url_app     = "http://app.spost.jp/";
	final private String _url_img_app = "http://img.spost.jp/";
	final private String _url_img_gen = "http://app.spost.jp/assets/img/common/icon/";

	// ＡＰＩ ディレクトリ
	final private String _api_file_mainte     = "global/";
	final private String _api_file_menu       = "switcher/";
	final private String _api_file_token      = "token/";
	final private String _api_file_notif      = "notification/";
	
	final private String _api_file_log        = "log/";
	final private String _api_file_action_cnt = "action/count/";

	final private String _api_file_app        = "app/";
	final private String _api_file_icon       = "icon/";	
	final private String _api_file_setting    = "setting/";
	
	// 他 URL
	final private String _url_ipost           = "http://i-post.jp/";
	final private String _url_hiropro         = "http://hiropro.co.jp/";
	final private String _url_hiropro_company = _url_hiropro + "#about";

	final private String _url_hp_faq   = "http://hiropro.sakura.ne.jp/ipost/faq.html";
	final private String _url_hp_howto = "http://hiropro.sakura.ne.jp/ipost/howto.html";
	
	// SharedPreference Name
	final private String _pref_name         = "api_data";
	final private String _pref_key_json     = "json";
	final private String _pref_key_mainte   = "mainte";
	final private String _pref_key_sidemenu = "sidemenu";
	final private String _pref_key_Loaded   = "loaded";
	
	// メール関連
	final private String _mail_title = "メールの件名";
	final private String _mail_text  = "メールの本文";
	
	// ＪＳＯＮデータ「ＳＮＳ項目」配列
	// ※１：格納されている順序ごとに表示
	// ※２：大文字、小文字に注意
	final private String[] socialList = {"facebook", "twitter", "line", "youtube"};
	
	/*
	 * ゲッター群 
	 */
	
	public String getClientID() {
		return _client_id;
	}
	public String getUrlApi() {
		return _url_api;
	}
	public String getUrlApp() {
		return _url_app;
	}
	public String getUrlImgGen() {
		return _url_img_gen;
	}
	public String getMailTitle() {
		return _mail_title;
	}
	public String getMailText() {
		return _mail_text;
	}
	public String[] getSocialList() {
		return socialList;
	}
	
	/*
	 * ＡＰＩ系のゲッター 
	 */
	
	/**
	 * メンテナンス情報の保有先ＵＲＬ返却
	 * http://api.spost.jp/global/CLIENT_ID/
	 * @return String:メンテナンス情報の保有先ＵＲＬ
	 */
	public String getApiMainte() {
		return _url_api + _api_file_mainte + _client_id + "/";
	}
	
	/**
	 * メニュー情報の保有先ＵＲＬ返却
	 * http://api.spost.jp/switcher/CLIENT_ID/
	 * @return String:メニュー情報の保有先ＵＲＬ
	 */
	public String getApiSidemenu() {
		return _url_api + _api_file_menu + _client_id + "/";
	}
	
	/**
	 * クライアント固有画像の保有先ＵＲＬ返却
	 * http://img.spost.jp/app/icon/CLIENT_ID/
	 * @return String:クライアント固有画像の保有先ＵＲＬ
	 */
	public String getApiImg() {
		return _url_img_app + _api_file_app + _api_file_icon + _client_id + "/";
	}
	
	/**
	 * Androidトークンの送信
	 * http://api.spost.jp/token/
	 * @return String
	 */
	public String getApiToken() {
		return _url_api + _api_file_token;
	}

	/**
	 * ログカウント送信
	 * http://api.spost.jp/log/action/cnt/
	 * @return String
	 */
	public String getApiLogActionCnt() {
		return _url_api + _api_file_log + _api_file_action_cnt;
	}
	
	/**
	 * セッティングAPI
	 * http://api.spost.jp/setting/CLIENT_ID/
	 * @return String
	 */
	public String getApiSetting() {
		return _url_api + _api_file_setting + _client_id + "/";
	}
	
	/**
	 * PUSH通知設定値送信先API
	 * http://api.spost.jp/setting/CLIENT_ID/notification/
	 * @return String
	 */
	public String getApiNotif() {
		return this.getApiSetting() + _api_file_notif;
	}
	
	/*
	 * 他URL
	 */
	// iPostのURLを返す
	public String getUrlIpost() {
		return _url_ipost;
	}
	// HIROPRO, Inc.のURLを返す
	public String getUrlHiropro() {
		return _url_hiropro;
	}
	// HIROPRO, Inc.の会社情報のURLを返す
	public String getUrlCompany() {
		return _url_hiropro_company;
	}
	// アプリFAQのURLを返す
	public String getUrlFaq() {
		return _url_hp_faq;
	}
	// アプリ使い方のURLを返す
	public String getHowto() {
		return _url_hp_howto;
	}
	
	/*
	 * SharedPreference Name
	 */
	public String getPrefKeyName(){
		return _pref_name;
	}
	public String getPrefKeyJson() {
		return _pref_key_json;
	}
	public String getPrefKeyMainte() {
		return _pref_key_mainte;
	}
	public String getPrefKeySidemenu() {
		return _pref_key_sidemenu;
	}
	public String getPrefKeyLoaded() {
		return _pref_key_Loaded;
	}
	
}
